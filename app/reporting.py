# -*- coding: utf-8 -*-
"""
Script pour générer du html à partir d'un fichier texte

Created on Wed Jul  8 21:35:23 2015

@author: thomas_a
"""
from __future__ import unicode_literals

import sys
import markdown
import webbrowser


class ReportWriter(object):
    """
    Capture et retranscrit tous les print() en langage Markdown pour pouvoir
    générer un rapport standardisé
    """

    def __init__(self, filename):
        self.__ori_stdout = sys.stdout
        self.stdout = sys.stdout
        self.__file = filename
        self.txtfile = None
        self.htmlfile = None
        self.open_(filename)

    def open_(self, filename, mode='w'):
        """
        Ouvre 2 fichiers (un en texte et l'autre en html)
        """
        self.txtfile = open(filename + '.txt', mode)
        self.htmlfile = open(filename + '.html', mode)
        self.__file = filename

    def write(self, text):
        """
        Ecrit ce qui est affiché à l'écran :
        - en texte
        - au format HTML
        """
        # self.stdout.write(text)
        self.txtfile.write(text)
        self.htmlfile.write(markdown.markdown(text,
                            extensions=['markdown.extensions.tables', ]))

    def close(self):
        # self.stdout.close()
        self.txtfile.close()
        self.htmlfile.close()
        self.txtfile = None
        self.htmlfile = None
        sys.stdout = self.__ori_stdout
        webbrowser.open("file://%s.html" % self.__file)

    def flush(self):
        """
        Vide tous les flux en cours
        """
        try:
            self.stdout.flush()
            self.txtfile.flush()
            self.htmlfile.flush()
        except ValueError:
            self.open_(self.__file, 'a')


def str_table2(_headers=None, _cellsize=20, **kwargs):
    if _headers is not None and len(_headers) != len(kwargs):
        logger.warning("Headers contains more column than kwargs contains \
            using kwargs.keys() instead of _headers param")
        _headers = None
    if _headers is None:
        _headers = list(kwargs.keys())
    max_rows = max([ len(v) for v in kwargs.values()])
    #Heads line
    res = "\n"+str_table_row(_headers, _cellsize)
    #Horizontal separator
    res += "\n"+str_table_row(
        [''.join(['-' * _cellsize]) for _ in range(len(kwargs))],
        _cellsize)
    for i in range(max_rows):
        line_arr = [("NODATA" if len(kwargs[k]) <= i else kwargs[k][i])
            for k in kwargs]
        res += "\n"+str_table_row(line_arr, _cellsize)
    return res

def str_table_row(lst, _cellsize):
    cellfmt = '%-{}s'.format(_cellsize)
    return '|'+('|'.join([cellfmt % i for i in lst]))+'|'

def str_table(header, size=20, **kwargs):
    """
        retrocompatibility wrapper
    """
    return str_table2(_headers = header, _cellsize = size, **kwargs)

def str_extend(txt, size = 20):
    """
        Etend une chaine de caractères au nombre de caractères demandés (size)
        en ajoutant des ' ' à la fin
    """
    return ('%%-%ds'%size)%txt
